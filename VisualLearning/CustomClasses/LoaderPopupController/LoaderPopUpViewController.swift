//
//  LoaderPopUpViewController.swift
//  Botler
//
//  Created by Archan on 15/02/19.
//  Copyright © 2019 Archan. All rights reserved.
//

import UIKit
var Globalimageheight = ""
var GlobalimageWidth = ""
class LoaderPopUpViewController: UIViewController {
    @IBOutlet weak var gifname: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
       // let imageView = UIImageView(image: jeremyGif)
        //imageView.frame = CGRect(x: 20.0, y: 50.0, width: self.view.frame.size.width - 40, height: 150.0)
        //view.addSubview(imageView)
        // Do any additional setup after loading the view.
    }
    
    weak var delegate: FooTwoViewControllerDelegate?

        @IBOutlet weak var btnok: UIButton!
        @IBOutlet weak var btndoubleviewright: UIButton!
        @IBOutlet weak var btndoubleviewleft: UIButton!
        @IBOutlet weak var popuptitle: UILabel!
        @IBOutlet weak var popupBackview: UIView!
        @IBOutlet weak var lblMsg: UILabel!
        @IBOutlet weak var btnOKview: UIView!
        @IBOutlet weak var BtnOkCancelview: UIView!
        @IBOutlet weak var popImageview: UIImageView!
        @IBOutlet weak var popupMainContenerView: UIView!
        @IBOutlet weak var popUpContenerView: UIView!
        @IBOutlet weak var popupRoundView: UIView!
        
        var didSelect: ((_ selectedItem: Any, _ error: String?) -> Void)?
        var statusType : StatusType!
        var viewControllertype : ViewcontrollerType!
        var apiCallType: ApiCallType!
        var parentVC = UIViewController()
        // MARK: - viewDidLayoutSubviews
        override func viewWillLayoutSubviews() {
            super.viewWillLayoutSubviews()
            
            // popupRoundView.roundedCorner(withCornerRadius: IS_IPAD() ? 150/2 : 100/2, borderWidth: 1.0, color: .white)
            //popUpContenerView.roundedCorner(withCornerRadius: 10, borderWidth: 1.0, color: .clear)
            
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
           // popupMainContenerView.alpha = 0
         //   popupBackview.alpha = 0
            popupMainContenerView.transform = CGAffineTransform(translationX: 0, y: -screenHeight)
            
        }
        
        
        func bounceFromTop() -> Void {
          
             self.popupBackview.fadeTo(0, duration: 0.1)

        }
        
        
        static func showPopup(onParentViewController parentViewController: UIViewController, withSelectedItem selectedItem: [Any],withapicallType apicallType: ApiCallType, withstatusType statusType: StatusType,withpopupType popupType: PopupviewType,withviewControllerType viewControllerType: ViewcontrollerType, selected: @escaping (_ value: Any?, _ index: String?) -> Void) {
            print("Selected Item==\(selectedItem)")
            let viewController =  UIStoryboard(name:"Main",bundle:nil)
            let obj1 = viewController.instantiateViewController(withIdentifier: "LoaderPopUpViewController") as! LoaderPopUpViewController
            obj1.statusType = statusType
            obj1.viewControllertype = viewControllerType
            obj1.displaylogoutPickerView(withParentViewController: parentViewController, withSelectedItem: selectedItem, withapicallType: apicallType, withstatusType: statusType, withpopupType: popupType, withviewControllerType: viewControllerType)
            obj1.didSelect = selected
            obj1.parentVC = parentViewController
        }
    
        func displaylogoutPickerView(withParentViewController parentController: UIViewController, withSelectedItem selectItem: [Any],withapicallType apicallType: ApiCallType,withstatusType statusType: StatusType, withpopupType popupType: PopupviewType,withviewControllerType viewControllerType: ViewcontrollerType ) {
            
            self.view.frame = UIScreen.main.bounds
            UIApplication.shared.windows.first!.addSubview(self.view)
            parentController.addChild(self)
            self.didMove(toParent: parentController)
            parentController.view.bringSubviewToFront(self.view)
            DispatchQueue.global().async {
                let image = UIImage.gif(name: "loader")
                DispatchQueue.main.async {
                    self.gifname.image = image
                }
            }
            bounceFromTop()
            self.viewControllertype = viewControllerType
            self.parentVC = parentController
                switch apicallType {
                case .Category:
                    getCategory()
                    break
                case .FindObject:
                    print("dashboard")
                    CategorylistApicall(withx: selectItem[0] as! Int64, withy: selectItem[1] as! Int64, withheight: selectItem[2] as! String, withwidth: selectItem[3] as! String)
                    break
                case .LandingVC:
                    GetOfflineImages()

                default:
                    print("wrong")
                
               }
        
    }
        private func dismissAnimationonPickerView() {
            
            UIView.animate(withDuration: 0.3, animations: {
                self.popUpContenerView.alpha = 0
                self.popupMainContenerView.alpha = 0
                self.popupBackview.alpha = 0
                self.popupMainContenerView.transform = CGAffineTransform(translationX: 0, y: screenHeight)
                
            }) { (_) in
                //            statusBarColorChange(withcolor: InvoiceScreenStatusColor)
                self.view.removeFromSuperview()
                self.removeFromParent()
            }
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        //ok button
}
private typealias APICalling = LoaderPopUpViewController
extension APICalling {
    
    func getCategory(){
        FetchData.GetMethodBaseApiCalling(withurlString: Urls.catagoryUrl, withSuccess: { (resp) in
            self.didSelect!(resp,"")
            self.dismissAnimationonPickerView()
        }) { (err) in
            print(err)
            print((err as! APIError).errorDetails)
            if ((err as! APIError).errorCode) == -200 || ((err as! APIError).errorCode) == -201 {
                
                self.delegate = self.parentVC as! FooTwoViewControllerDelegate
                self.delegate?.myVCDidFinish(error: "")
            }
            no_internet = true
            self.dismissAnimationonPickerView()
        }
    }
    
    
    private func GetOfflineImages() {
      
        ApiCall.BaseApiCalling(withurlString: Urls.offline_images, withjsonString: "{\"height\":\"\(Globalimageheight)\",\"width\":\"\(GlobalimageWidth)\",\"per_page_item\":\"2\",\"page_no\":\"1\"}", withloaderMessage: "loading", withSuccess: { (resp) in
            print(resp)
            self.dismissAnimationonPickerView()
            let Main_response = resp as! [String: Any]
            let status  = Main_response["status"] as! NSNumber
            if status == 0 {
                print(resp)
                if let Response = Main_response["Response"] as? [Any]{
                    if Response.count != 0 {
                        self.didSelect!(resp,"")
                       
                    }else{
                        self.didSelect!("NO Data","")
                        self.dismissAnimationonPickerView()

                    }
                    
                }
                
                
                
            } else  if status == 1 {
                
                if let response = Main_response["response"] as? [String:Any]{
                    if let message = response["message"] as? String{
                        
                    }
                    
                }
                
            }  else {
            }
        }) { (err) in
            print("*******\n\n\n\n\n******",err)
            self.didSelect!(err,"error")
            self.dismissAnimationonPickerView()

        }
    }
    
private func CategorylistApicall(withx x: Int64,withy y:Int64, withheight height:String,withwidth width:String) {
    FindObjectApiCall(withx: x, withy: y, withheight: height, withwidth: width)
    
}
    func FindObjectApiCall(withx x: Int64,withy y:Int64, withheight height:String,withwidth width:String) {
        ApiCall.BaseApiCalling(withurlString: Urls.detectObjectUrl, withjsonString: "{\"ext\":\"\(imagextension)\",\"name\":\"\(imagename)\",\"objectName\":\"\(objectname)\",\"height\":\"\(height)\",\"width\":\"\(width)\",\"flag\":\"\(objectflag)\",\"xAxis\":\"\(x)\",\"yAxis\":\"\(y)\"}", withloaderMessage: "loading", withSuccess: { (resp) in
            print(resp)
            if let Response = resp as? [String:Any]{
                    //     self.img!.sd_setImage(with: URL(string: "http://3.15.224.14:8000/static/objects/apple_3.jpg"), placeholderImage: UIImage(named: ""))
                    if (Response["status"] as! String) == "Success !!!" {
                        self.didSelect!(Response,"")
                        self.dismissAnimationonPickerView()

                        
                        
                    }else if (Response["status"] as! String) == "Error!! Outside bounding Box" {
                        self.didSelect!(Response,"Error!! Outside bounding Box")
                        self.dismissAnimationonPickerView()
                        
                        
                        
                    }
                
                
            }
            
            
            
            
            
        }) { (err) in
                self.delegate = self.parentVC as! FooTwoViewControllerDelegate
                self.delegate?.myVCDidFinish(error: "")
            no_internet = true
            self.dismissAnimationonPickerView()
        }
    }
}




