//
//  Constants.swift
//  Pedometer
//
//  Created by DebasmitaArchan on 02/07/18.
//  Copyright © 2018 MAC2. All rights reserved.
//

import Foundation
import UIKit

var AUTHKEY = "FTN20190211_BU93HBVF0"
public let LoginScreenStatusColor = UIColorRGBA(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.6)
public let ScreenRedColor = UIColorRGB(red: 255.0, green: 0.0, blue: 0.0)
public let ScreenBlueColor = UIColorRGB(red: 13.0, green: 89.0, blue: 180.0)
public let ScreenGrayColor = UIColorRGB(red: 114.0, green: 118.0, blue: 114.0)
public let screenWidth = UIScreen.main.bounds.width
public let screenHeight = UIScreen.main.bounds.height
public var myProfileContent : [String:Any]? = nil
public let ImageEntity = "Images"
public let ObjectEntity = "Objects"
public let CategoryEntity = "Category"
struct APIError: Error {
    var errorCode: Int?
    var errorDetails: String?
}

struct Section {
    
    var name: String!
    var items: [String]!
    var images: [String]!
    var text : [String]!
    
    init(name: String?, items: [String], images: [String], text: [String]) {
        
        self.name = name
        self.items = items
        self.images = images
        self.text = text
        
    }
}


var locationaddress = ""

enum PostType {
    case postProperty
    case postCar
    case realState
    case none
}

enum ViewcontrollerType {
   case CategoryVC
    case FindObjectVC
    case LandingVC

    
}
enum StatusType {
    case Nointernet
    case Nodata
    case Other
    
}

enum PopupviewType {
    case Single         //for only Ok button
    case Double         //for Ok and cancel button
    
}



enum ApiCallType {
    case Category
   case FindObject
    case LandingVC

}
protocol FooTwoViewControllerDelegate:class {
    func myVCDidFinish( error: String)
}

extension UIView {
    enum GlowEffect: Float {
        case small = 0.4, normal = 2, big = 15
    }
    
    func doGlowAnimation(withColor color: UIColor, withEffect effect: GlowEffect = .big) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowRadius = 0
        layer.shadowOpacity = 5
        layer.shadowOffset = .zero
        
        let glowAnimation = CABasicAnimation(keyPath: "shadowRadius")
        glowAnimation.fromValue = 0
        glowAnimation.toValue = effect.rawValue
        glowAnimation.beginTime = CACurrentMediaTime()+0.3
        glowAnimation.duration = CFTimeInterval(0.3)
        glowAnimation.fillMode = .removed
        glowAnimation.autoreverses = true
        glowAnimation.isRemovedOnCompletion = true
        layer.add(glowAnimation, forKey: "shadowGlowingAnimation")
    }
}

extension UIImageView {
    var contentClippingRect: CGRect {
        guard let image = image else { return bounds }
        guard contentMode == .scaleAspectFit else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }

        let scale: CGFloat
        if image.size.width > image.size.height {
            scale = bounds.width / image.size.width
        } else {
            scale = bounds.height / image.size.height
        }

        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let x = (bounds.width - size.width) / 2.0
        let y = (bounds.height - size.height) / 2.0

        return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
}

extension UIView {
    func shake() {
        self.transform = CGAffineTransform(translationX: 20, y: 0)
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
}
func downalod(){
    if let audioUrl = URL(string: "http://3.21.186.178:8000/static/objects/audio_files/banana.mp3") {
               
               // then lets create your document folder url
               let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
               
               // lets create your destination file url
               let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
               print(destinationUrl)
               
               // to check if it exists before downloading it
               if FileManager.default.fileExists(atPath: destinationUrl.path) {
                   print("The file already exists at path")
                   
                   // if the file doesn't exist
               } else {
                   
                   // you can use NSURLSession.sharedSession to download the data asynchronously
                   URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                       guard let location = location, error == nil else { return }
                       do {
                           // after downloading your file you need to move it to your destination url
                           try FileManager.default.moveItem(at: location, to: destinationUrl)
                           print("File moved to documents folder")
                        //playmusic()
                       } catch let error as NSError {
                           print(error.localizedDescription)
                       }
                   }).resume()
               }
           }
}
