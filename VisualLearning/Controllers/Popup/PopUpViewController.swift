//
//  PopUpViewController.swift
//  KazziBalance
//
//  Created by new1 on 07/05/18.
//  Copyright © 2018 Karmick Solutions. All rights reserved.
//

import UIKit


class PopUpViewController: UIViewController {
    
    @IBOutlet weak var btnnointernet: UIButton!
    @IBOutlet weak var gifimage: UIImageView!
    @IBOutlet weak var lblmsg: UILabel!
    @IBOutlet weak var popupBackview: UIView!
    @IBOutlet weak var popupMainContenerView: UIView!
    @IBOutlet weak var popUpContenerView: UIView!
    
    var didSelect: ((_ selectedItem: String, _ index: Int?) -> Void)?
    var statusType : StatusType!
     var viewControllerType : ViewcontrollerType!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        popupBackview.addGestureRecognizer(tap)
        popupBackview.isUserInteractionEnabled = true

        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        didSelect!("try again",0)

            dismissAnimationonPickerView()
        
    }
    
    @IBAction func clicktryagain(_ sender: Any) {
        didSelect!("try again",0)
        dismissAnimationonPickerView()

    }
    
    // MARK: - viewDidLayoutSubviews
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        // Or to rotate and lock
        AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)

       // popupRoundView.roundedCorner(withCornerRadius: IS_IPAD() ? 150/2 : 100/2, borderWidth: 1.0, color: .white)
        //popUpContenerView.roundedCorner(withCornerRadius: 10, borderWidth: 1.0, color: .clear)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        popupMainContenerView.alpha = 0
        popupBackview.alpha = 0
        popupMainContenerView.transform = CGAffineTransform(translationX: 0, y: -screenHeight)
        
    }
    override open var shouldAutorotate: Bool {
        return false
    }
    
  
  
    // Specify the orientation.
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }
    
    func bounceFromTop() -> Void {
        
        
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions(rawValue: 0), animations: {
            self.popupMainContenerView.alpha = 1.0
            self.popupBackview.alpha = 0.8
            self.popupMainContenerView.transform = CGAffineTransform.identity
            
        }, completion: nil)
    }
    
    
    static func showPopup(onParentViewController parentViewController: UIViewController, withSelectedItem selectedItem: String?,withalertTitle alertTitle:String, withstatusType statusType: StatusType,withpopupType popupType: PopupviewType,withviewControllerType viewControllerType: ViewcontrollerType, selected: @escaping (_ value: String?, _ index: Int?) -> Void) {
        
        print("Selected Item==\(selectedItem!)")
        let viewController =  UIStoryboard(name:"Main",bundle:nil)
        let obj1 = viewController.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
        obj1.statusType = statusType
        obj1.viewControllerType = viewControllerType
        obj1.displaylogoutPickerView(withParentViewController: parentViewController, withSelectedItem: selectedItem, withalertTitle: alertTitle, withstatusType: statusType, withpopupType: popupType, withviewControllerType: viewControllerType)
        obj1.didSelect = selected
        
    }
    func displaylogoutPickerView(withParentViewController parentController: UIViewController, withSelectedItem selectItem: String?,withalertTitle alertTitle:String,withstatusType statusType: StatusType, withpopupType popupType: PopupviewType,withviewControllerType viewControllerType: ViewcontrollerType ) {
        
        self.view.frame = UIScreen.main.bounds
        UIApplication.shared.windows.first!.addSubview(self.view)
        parentController.addChild(self)
        self.didMove(toParent: parentController)
        parentController.view.bringSubviewToFront(self.view)
        
        if statusType == .Nointernet {
        DispatchQueue.global().async {
            let image = UIImage.gif(name: "no-connection")
            DispatchQueue.main.async {
                self.gifimage.image = image
                self.bounceFromTop()
                self.btnnointernet.setTitle("Something went wrong!", for: .normal)
            }
        }
        }else{
            DispatchQueue.global().async {
                let image = UIImage.gif(name: "searching")
                DispatchQueue.main.async {
                    self.gifimage.image = image
                    self.bounceFromTop()
                    self.btnnointernet.setTitle("We are still searching for you!", for: .normal)
                    self.popupMainContenerView.backgroundColor = #colorLiteral(red: 0.4941176471, green: 0.8431372549, blue: 0.8823529412, alpha: 1)
                    self.gifimage.backgroundColor = #colorLiteral(red: 0.4941176471, green: 0.8431372549, blue: 0.8823529412, alpha: 1)
                }
            }
        }
        
        
    }
    
    private func dismissAnimationonPickerView() {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.popUpContenerView.alpha = 0
            self.popupMainContenerView.alpha = 0
            self.popupBackview.alpha = 0
            self.popupMainContenerView.transform = CGAffineTransform(translationX: 0, y: screenHeight)
           
        }) { (_) in
//            statusBarColorChange(withcolor: InvoiceScreenStatusColor)
            self.view.removeFromSuperview()
            self.removeFromParent()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //ok button
   
 
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
