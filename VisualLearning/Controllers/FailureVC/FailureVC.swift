//
//  FailureVC.swift
//  Kidsapp
//
//  Created by DebasmitaArchan on 16/02/20.
//  Copyright © 2020 DebasmitaArchan. All rights reserved.
//

import UIKit
import UICircularProgressRing
class FailureVC: UIViewController {
    @IBOutlet weak var btnok: UIButton!
    @IBOutlet weak var btndoubleviewright: UIButton!
    @IBOutlet weak var btndoubleviewleft: UIButton!
    @IBOutlet weak var popuptitle: UILabel!
    @IBOutlet weak var popupBackview: UIView!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var btnOKview: UIView!
    @IBOutlet weak var BtnOkCancelview: UIView!
    @IBOutlet weak var popImageview: UIImageView!
    @IBOutlet weak var popupMainContenerView: UIView!
    @IBOutlet weak var popUpContenerView: UIView!
    @IBOutlet weak var popupRoundView: UIView!
    @IBOutlet weak var loaderview: UICircularProgressRing!

    var didSelect: ((_ selectedItem: Any, _ error: String?) -> Void)?
    var statusType : StatusType!
    var viewControllerType : ViewcontrollerType!
    var apiCallType: ApiCallType!
    var parentVC = UIViewController()
    // MARK: - viewDidLayoutSubviews
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
    

    // Specify the orientation.
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        popupMainContenerView.alpha = 0
        //   popupBackview.alpha = 0
        popupMainContenerView.transform = CGAffineTransform(translationX: 0, y: -screenHeight)
        // Or to rotate and lock
        AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)

    }
    
    
    func bounceFromTop() -> Void {
        
        self.popupBackview.fadeTo(0, duration: 0.1)
        
    }
    
    
    static func showPopup(onParentViewController parentViewController: UIViewController, withSelectedItem selectedItem: String?,withapicallType apicallType: ApiCallType, withstatusType statusType: StatusType,withpopupType popupType: PopupviewType,withviewControllerType viewControllerType: ViewcontrollerType, selected: @escaping (_ value: Any?, _ index: String?) -> Void) {
        
        print("Selected Item==\(selectedItem!)")
        let viewController =  UIStoryboard(name:"Main",bundle:nil)
        let obj1 = viewController.instantiateViewController(withIdentifier: "FailureVC") as! FailureVC
        obj1.statusType = statusType
        obj1.viewControllerType = viewControllerType
        obj1.displaylogoutPickerView(withParentViewController: parentViewController, withSelectedItem: selectedItem, withapicallType: apicallType, withstatusType: statusType, withpopupType: popupType, withviewControllerType: viewControllerType)
        obj1.didSelect = selected
        obj1.parentVC = parentViewController
    }
    
    func displaylogoutPickerView(withParentViewController parentController: UIViewController, withSelectedItem selectItem: String?,withapicallType apicallType: ApiCallType,withstatusType statusType: StatusType, withpopupType popupType: PopupviewType,withviewControllerType viewControllerType: ViewcontrollerType ) {
        
        self.view.frame = UIScreen.main.bounds
        UIApplication.shared.windows.first!.addSubview(self.view)
        parentController.addChild(self)
        self.didMove(toParent: parentController)
        parentController.view.bringSubviewToFront(self.view)
        
     //   self.gifname.image = UIImage.gifImageWithName("glass_loader")
        bounceFromTop()
        
        if apicallType == .FindObject {
            if no_internet == true {
                no_internet = false
                self.dismissAnimationonPickerView()
                
            }
            self.popImageview.isHidden = true
            self.loaderview.style = .bordered(width: 3.0, color: #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1))
            self.loaderview.isHidden = false
            self.loaderview.continueProgress()
              DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.loaderview.startProgress(to: 100.0, duration: 100.0) {
                self.didSelect!("MostRecent","")
                self.dismissAnimationonPickerView()
            }
            }
        }else {
            self.loaderview.isHidden = true
            DispatchQueue.global().async {
                let image = UIImage.gif(name: "confused_1")
                DispatchQueue.main.async {
                    self.popImageview.image = image
                }
            }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.didSelect!("MostRecent","")
            self.dismissAnimationonPickerView()
        }
        }
        
    }
    
    private func dismissAnimationonPickerView() {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.popUpContenerView.alpha = 0
            self.popupMainContenerView.alpha = 0
            self.popupBackview.alpha = 0
            self.popupMainContenerView.transform = CGAffineTransform(translationX: 0, y: screenHeight)
            
        }) { (_) in
            //            statusBarColorChange(withcolor: InvoiceScreenStatusColor)
            self.view.removeFromSuperview()
            self.removeFromParent()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //ok button
   
    
    

}
