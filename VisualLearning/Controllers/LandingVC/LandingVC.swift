//
//  LandingVC.swift
//  Kidsapp
//
//  Created by DebasmitaArchan on 18/01/20.
//  Copyright © 2020 DebasmitaArchan. All rights reserved.
//

import UIKit
import AVFoundation

class LandingVC: UIViewController{
    
    @IBOutlet weak var btnoffline: UIButton!
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var age1: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        age1.shake()
        print(UserDefaults.standard.value(forKey: "offline"))
        Globalimageheight = (Float(self.img.frame.height).cleanValue)
        GlobalimageWidth = (Float(self.img.frame.width).cleanValue)
        print((self.img.frame.height),(self.img.frame.width),(self.img.frame.origin.x),(self.img.frame.origin.y))
                print(GlobalimageWidth)

    }
    @IBAction func clickexit(_ sender: Any) {
        exit(0)
    }
    override open var shouldAutorotate: Bool {
        return false
    }
    var players = AVPlayer()
    var arrayOfCategory = [Any]()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let url = Bundle.main.url(forResource: "CHEERFUL_THEME_A_j_j_schutz_HIB307_LC06881", withExtension: "mp3")
        let playerItem = AVPlayerItem(url: url!)
        players = AVPlayer(playerItem: playerItem)
        let playerLayer=AVPlayerLayer(player: players)
        playerLayer.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        self.view.layer.addSublayer(playerLayer)
        
       // players.play()
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object:
        players.currentItem, queue: .main) { [weak self] _ in
            self?.players.seek(to: CMTime.zero)
          //  self?.players.play()
        }
      //  AppUtility.lockOrientation(.landscape)
        // Or to rotate and lock
         AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)

        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.players.pause()
        if UserDefaults.standard.value(forKey: "offline") == nil {
           // self.btnoffline.setTitle("Offline", for: .normal)
        }else if UserDefaults.standard.bool(forKey: "offline") == true {
           // self.btnoffline.setTitle("Online", for: .normal)

        }else {
           // self.btnoffline.setTitle("Offline", for: .normal)

        }

    }
    
    func updateProgressView(){
       
   }
    
    // Specify the orientation.
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }
    @IBAction func clickchild(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "offline")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let mDashBoardViewController = storyBoard.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
        self.navigationController?.pushViewController(mDashBoardViewController, animated: true)
    }
    
    @IBAction func clickoffline(_ sender: Any) {
      
        
             var myarr = [Category]()
            myarr = CoreDataHelper.retrieveData(withEntityname: CategoryEntity, withcolumnname: "")
            if myarr.count == 0 {
            getOfflinedata()
            
        }else{
            UserDefaults.standard.set(true, forKey: "offline")
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                   let mDashBoardViewController = storyBoard.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
            mDashBoardViewController.arrayOfCategory = myarr
                   self.navigationController?.pushViewController(mDashBoardViewController, animated: true)
        }

        
        }

}
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


struct AppUtility {
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        
        self.lockOrientation(orientation)
        
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }
    
}

private typealias ApiCalling = LandingVC
extension ApiCalling {
    func getOfflinedata(){
        LoaderPopUpViewController.showPopup(onParentViewController: self, withSelectedItem: [], withapicallType: .LandingVC, withstatusType: .Other, withpopupType: .Single, withviewControllerType: .LandingVC) { (resp, err) in
            if err!.count == 0 {
                print(resp)
                if  let Main_response = resp as? [String: Any] {
                let status  = Main_response["status"] as! NSNumber
                if status == 0 {
                    if let Response = Main_response["Response"] as? [Any]{
                        if Response.count != 0 {
                            FailureVC.showPopup(onParentViewController: self, withSelectedItem: "", withapicallType: .FindObject, withstatusType: .Other, withpopupType: .Double, withviewControllerType: .CategoryVC) { (_, _) in
                                
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 50.0) {
                                
                                                                                  self.arrayOfCategory = Response
                                                                                    CoreDataHelper.deleteData(withEntityname: CategoryEntity)
                                                                                    CoreDataHelper.createDataForCategoryEntity(withCategorydata: self.arrayOfCategory)
                                                                                    var myarr = [Category]()
                                                                                   myarr = CoreDataHelper.retrieveData(withEntityname: CategoryEntity, withcolumnname: "")
                                                                                    if myarr.count != 0 {
                                                                                        UserDefaults.standard.set(true, forKey: "offline")
                                                                                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                                                                               let mDashBoardViewController = storyBoard.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
                                                                                        mDashBoardViewController.arrayOfCategory = myarr
                                                                                               self.navigationController?.pushViewController(mDashBoardViewController, animated: true)
                                                                                    }
                                
                                
                            }
                            
                        }
                    }
                } else  if status == 1 {
                    if let response = Main_response["response"] as? [String:Any]{
                        if let message = response["message"] as? String{
                            
                        }
                        
                    }
                    
                }  else {
                }
                }else{
                    PopUpViewController.showPopup(onParentViewController: self, withSelectedItem: "", withalertTitle: "", withstatusType: .Nointernet, withpopupType: .Double, withviewControllerType: .CategoryVC, selected: { (_, _) in
                    })
                }
            }else{
                    PopUpViewController.showPopup(onParentViewController: self, withSelectedItem: "", withalertTitle: "", withstatusType: .Nointernet, withpopupType: .Double, withviewControllerType: .CategoryVC, selected: { (_, _) in
                    })
                
            }
        }
    }
}
