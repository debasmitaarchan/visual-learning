//
//  FindObjectVC.swift
//  Kidsapp
//
//  Created by DebasmitaArchan on 18/01/20.
//  Copyright © 2020 DebasmitaArchan. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import CoreData
var imagename = String()
var imagextension = String()
var objectname = String()
var objectflag = String()
class FindObjectVC: UIViewController ,FooTwoViewControllerDelegate {
    var managedObjectContext: NSManagedObjectContext? = nil
    weak var delegate: FooTwoViewControllerDelegate?
    var cat_id = NSNumber()
    var cat_type = String()
    var arrcategory = [Any]()
    var clickflag = false
    var Online = false
    var player : AVPlayer?
    let imageCache = SDImageCache()
    var objectcount = 0
    var imgObj = [String:Any]()
    var imgOfflineObj : Images?
    var offline_text_to_speech = NSData()
    var currentOfflineObject : Objects?
    var text_to_speech = String()
    var objectEndFlag = false
    var offline_box_view = UIView()
    
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
    @IBOutlet weak var btnsettings: UIButton!
    @IBOutlet weak var btnhome: UIButton!
    @IBOutlet weak var btnexit: UIButton!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var boxview: UIViewX!
    var imagecount = Int()
    var currentcount = 0
    var wrongCount = 0
    var arrImageResponse = [Any]()
    var arrOfflineImages : [Images]?
    var arrObjectList = [Any]()
    var arrOfObjects : [Objects]?
    var arrOfTotalObjects : [[Objects]]?
    var arrMp3List = [Any]()
    var arrOftexttoSpeech = [Any]()
    var imageHeight = ""
    var imageWidth = ""
    @IBOutlet weak var backview: UIViewX!
    var myPlayer = AVAudioPlayer()
    var yourSound:NSURL?
    func prepareYourSound(myData:NSData) {
        do {
        myPlayer = try AVAudioPlayer(data: myData as Data, fileTypeHint: AVFileType.mp3.rawValue)
        myPlayer.prepareToPlay()
        }catch let error as NSError {
            ////print(error.localizedDescription)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print(screenHeight,"*****", screenWidth)

        if  UserDefaults.standard.value(forKey: "offline") != nil {
                   if UserDefaults.standard.bool(forKey: "offline") == false {
                    self.Online = true
                    GetImages(withx: 0, withy: 0)
                   }else {
                    self.Online = false
                    self.GetOfflineImages()
            }
        }
        ////print(self.backview.frame.height)
        self.backview.layer.cornerRadius = self.backview.frame.height / 2;
       
    }
    override open var shouldAutorotate: Bool {
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       // AppUtility.lockOrientation(.landscape)
        // Or to rotate and lock
        AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)

    }
    
    // Specify the orientation.
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }
    func loadRadio(radioURL: String) {
        //kids_cheering
        guard let url = URL.init(string: radioURL) else { return }
        let playerItem = AVPlayerItem.init(url: url)
        player = AVPlayer.init(playerItem: playerItem)
        player?.play()
            self.view.isUserInteractionEnabled = true
        

    }
    func myVCDidFinish(error: String) {
        self.view.isUserInteractionEnabled = true
        PopUpViewController.showPopup(onParentViewController: self, withSelectedItem: "", withalertTitle: "", withstatusType: .Nointernet, withpopupType: .Double, withviewControllerType: .CategoryVC) { (_, _) in
            self.view.isUserInteractionEnabled = true

        }
    }
    func loadGong(){
        let url = Bundle.main.url(forResource: "Cartoon-game-ending", withExtension: "mp3")
        
        let playerItem = AVPlayerItem(url: url!)
        let player=AVPlayer(playerItem: playerItem)
        let playerLayer=AVPlayerLayer(player: player)
        
        playerLayer.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        self.view.layer.addSublayer(playerLayer)
        player.play()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.loadTryAgain()
        }
        
    }
    
    func loadTryAgain(){
        let url = Bundle.main.url(forResource: "preview", withExtension: "mp3")
        let playerItem = AVPlayerItem(url: url!)
        let player=AVPlayer(playerItem: playerItem)
        let playerLayer=AVPlayerLayer(player: player)
        playerLayer.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        self.view.layer.addSublayer(playerLayer)
        player.play()
        
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: img)
            // do something with your currentPoint
            ////print(currentPoint)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: img)
            // do something with your currentPoint
            
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: img)
            if self.Online == true {
            FindObjectApicall(withx: Int64(currentPoint.x), withy: Int64(currentPoint.y), withheight: (Float(self.img.frame.height).cleanValue), withwidth: (Float(self.img.frame.width).cleanValue))
            }else{
                let touchpoint:CGPoint = touch.location(in: self.img)
                let views = self.offline_box_view.subviews[0]
                let rect1 = CGRect(x: views.frame.origin.x, y: views.frame.origin.y, width: views.frame.size.width, height: views.frame.size.height)
                   if rect1.contains(touchpoint) {
                       ////print("Inside")
                    //MARK:- Success Case
//                     self.createQuestions { () -> () in
//                            self.newQuestion()
//                        }
//


                   
                    SuccessVC.showPopup(onParentViewController: self, withSelectedItem: "", withalertTitle: "Sorry!", withstatusType: .Other, withpopupType: .Single, withviewControllerType: .CategoryVC, selected: { (_, _) in
                        self.view.isUserInteractionEnabled = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        if self.objectEndFlag == true {
                        self.currentcount = self.currentcount + 1
                            self.loadImage(withindex: self.currentcount, withimage: self.arrImageResponse, withOnline: self.Online, withOfflineImage: self.arrOfflineImages!)
                        }else{
                            self.loadimageObject(withimageobj: self.imgObj, withOnline: self.Online, withOfflineImageObj: [self.imgOfflineObj!] )
                        }
                        
                        }
                        })

                        self.offline_box_view.removeFromSuperview()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            let url = Bundle.main.url(forResource: "kids_cheering", withExtension: "mp3")
                            let playerItem = AVPlayerItem(url: url!)
                            let player=AVPlayer(playerItem: playerItem)
                            let playerLayer=AVPlayerLayer(player: player)
                            
                            playerLayer.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
                            self.view.layer.addSublayer(playerLayer)
                            player.play()
                    }

                            
                
                    
                    
                    
                   } else {
                       ////print("Outside")
                    //MARK:- Wrong Case

                    self.wrongCount = self.wrongCount + 1
                    if self.wrongCount == 3 {
                        self.FailureAttempt(withboundingboxarr: [], withOnline: false, withOfflineBoundingBox: [])
                        
                    }else {
                        self.loadGong()
                    FailureVC.showPopup(onParentViewController: self, withSelectedItem: "", withapicallType: .Category, withstatusType: .Other, withpopupType: .Single, withviewControllerType: .CategoryVC, selected: { (_, _) in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                        self.FailureAttempt(withboundingboxarr: [], withOnline: false, withOfflineBoundingBox: [])
                        }

                    })
                    }
                    
                   }
                

            }
        }
    }
    @IBAction func clicksettings(_ sender: Any) {
        if clickflag == false {
           clickflag = true
            btnexit.isHidden = false
            btnhome.isHidden = false
        }else{
            clickflag = false
            btnexit.isHidden = true
            btnhome.isHidden = true
        }
    }
    @IBAction func clikchome(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let mDashBoardViewController = storyBoard.instantiateViewController(withIdentifier: "LandingVC") as!
        LandingVC
        self.navigationController?.pushViewController(mDashBoardViewController, animated: true)
    }
    
    @IBAction func clickexit(_ sender: Any) {
        exit(0)
    }
    @IBAction func clickback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func loadLaert(){
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        self.present(alert, animated: true, completion: nil)

    }
    func dismisALert(){
        self.dismiss(animated: false, completion: nil)
    }
}
private typealias ApiCalling = FindObjectVC
extension ApiCalling {
    
    private func FindObjectApicall(withx x: Int64,withy y:Int64, withheight height:String,withwidth width:String){
        //self.view.isUserInteractionEnabled = false
        LoaderPopUpViewController.showPopup(onParentViewController: self, withSelectedItem: [x,y,height,width], withapicallType: .FindObject, withstatusType: .Other, withpopupType: .Single, withviewControllerType: .FindObjectVC)
        { (response, errors) in
            //MARK:- Success Case
            if errors?.count == 0 {
                
                if let Response = response as? [String:Any]{
                        if (Response["status"] as! String) == "Success !!!" {
                            let url = Bundle.main.url(forResource: "kids_cheering", withExtension: "mp3")
                            let playerItem = AVPlayerItem(url: url!)
                            let player=AVPlayer(playerItem: playerItem)
                            let playerLayer=AVPlayerLayer(player: player)
                            
                            playerLayer.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
                            self.view.layer.addSublayer(playerLayer)
                            player.play()
                            
                            
                            SuccessVC.showPopup(onParentViewController: self, withSelectedItem: errors, withalertTitle: "Sorry!", withstatusType: .Other, withpopupType: .Single, withviewControllerType: .CategoryVC, selected: { (_, _) in
                                self.view.isUserInteractionEnabled = true
                                if self.objectEndFlag == true {
                                self.currentcount = self.currentcount + 1
                                    self.loadImage(withindex: self.currentcount, withimage: self.arrImageResponse, withOnline: self.Online, withOfflineImage: [])
                                }else{
                                   self.loadimageObject(withimageobj: self.imgObj, withOnline: self.Online, withOfflineImageObj: [] )
                                }
                                })
                                
                        }
                    
                    
                }
                
            }else {
                //MARK:- Wrong Case
                ////print("erro8**********",errors,response)
                self.wrongCount = self.wrongCount + 1

                if self.wrongCount == 3 {
                    if let respns =  response as? [String:Any] {
                        let boundingBoxRightBottom = respns["boundingBoxRightBottom"] as! String
                        let boundingBoxLeftTop = respns["boundingBoxLeftTop"] as! String
                        if let boundingBoxes = respns["boundingBoxes"] as? [Any] {
                            self.FailureAttempt(withboundingboxarr: boundingBoxes, withOnline: self.Online, withOfflineBoundingBox: [])
                        }
                        
                    }
                    
                }else {
                    self.loadGong()
                FailureVC.showPopup(onParentViewController: self, withSelectedItem: "", withapicallType: .Category, withstatusType: .Other, withpopupType: .Single, withviewControllerType: .CategoryVC, selected: { (_, _) in
                    if let respns =  response as? [String:Any] {
                        let boundingBoxRightBottom = respns["boundingBoxRightBottom"] as! String
                        let boundingBoxLeftTop = respns["boundingBoxLeftTop"] as! String
                        if let boundingBoxes = respns["boundingBoxes"] as? [Any] {
                            self.FailureAttempt(withboundingboxarr: boundingBoxes, withOnline: self.Online, withOfflineBoundingBox: [])
                        }

                    }
                })
                }
            }
        }
    }
    


    //MARK:- GET Online Images
        private func GetImages(withx x: Int64,withy y:Int64) {
            self.imageHeight = ((Float(self.img.frame.height).cleanValue))
            self.imageWidth = ((Float(self.img.frame.width).cleanValue))
            print(self.boxview.frame.height,self.boxview.frame.width)
            ////print((Float(self.img.frame.height).cleanValue))
            ApiCall.BaseApiCalling(withurlString: Urls.getimageUrl, withjsonString: "{\"height\":\"\(Float(self.img.frame.height).cleanValue)\",\"width\":\"\((Float(self.img.frame.width).cleanValue))\",\"cat_id\":\"\(cat_id)\"}", withloaderMessage: "loading", withSuccess: { (resp) in
                ////print(resp)
                let Main_response = resp as! [String: Any]
                let status  = Main_response["status"] as! NSNumber
                if status == 200 {
                    if let Response = Main_response["Response"] as? [Any]{
                        if Response.count != 0 {
                            self.currentcount = 0
                            self.imagecount = (((Main_response["Response"] as? [Any])?.count)!)
                            self.arrImageResponse = Response
                            self.loadLaert()
                            self.loadImage(withindex: 0, withimage: Response, withOnline: self.Online, withOfflineImage: [])
                           
                        }
                        
                    }else {
                        if let message  = Main_response["Message"] as? String {
                            if message == "No Data Available!!!" {
                                PopUpViewController.showPopup(onParentViewController: self, withSelectedItem: "", withalertTitle: "", withstatusType: .Nodata, withpopupType: .Double, withviewControllerType: .CategoryVC, selected: { (_, _) in
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                        }
                    }
                    
                    
                    
                } else  if status == 1 {
                    
                    if let response = Main_response["response"] as? [String:Any]{
                        if let message = response["message"] as? String{
                            
                        }
                        
                    }
                    
                }  else {
                }
            }) { (err) in
                ////print(err)
            }
        }
}

private typealias OnlineExtensions = FindObjectVC
extension OnlineExtensions {

    //MARK: - ############################################### O N L I N E ####################################
    
    //MARK: - ############################################### O N L I N E  METHOD 1 ####################################
    func loadImage(withindex imageindex: Int, withimage images: [Any], withOnline online: Bool,withOfflineImage OfflineImage:[Images]){
        
        self.offline_box_view.removeFromSuperview()
        switch online {
        case true:
            //MARK: - Load MAIN IMAGE Online

            self.objectEndFlag = false
            self.view.isUserInteractionEnabled = true
            self.wrongCount = 0
            self.objectcount = 0
            if self.currentcount < images.count {
                let imageobj = images[self.currentcount] as! [String:Any]
                self.imgObj = images[self.currentcount] as! [String:Any]
                loadimageObject(withimageobj: imageobj, withOnline: self.Online, withOfflineImageObj: [])
            }else {
                self.currentcount = 0
                loadImage(withindex: 0, withimage: self.arrImageResponse, withOnline: self.Online, withOfflineImage: [])

            }
            break
            
        case false:
            //MARK: - Load MAIN IMAGE Offline

            self.objectEndFlag = false
            self.view.isUserInteractionEnabled = true
            self.wrongCount = 0
            self.objectcount = 0
            if self.currentcount < OfflineImage.count {
                self.imgOfflineObj = OfflineImage[self.currentcount] as! Images
                loadimageObject(withimageobj: [:], withOnline: self.Online, withOfflineImageObj: [self.imgOfflineObj!])
            }else {
                self.currentcount = 0
                loadImage(withindex: 0, withimage: self.arrImageResponse, withOnline: self.Online, withOfflineImage: self.arrOfflineImages!)

            }
            break
            ////print("")
        }
        
        
        
        
    }
    
    //MARK: - ############################################### O N L I N E  METHOD 2 ####################################

    private func loadimageObject(withimageobj imageobj:[String:Any],withOnline online:Bool,withOfflineImageObj OfflineimageObj: [Images]){
        
        
        switch online {
        case true:
            //MARK: - Load Online Images

                imagename = imageobj["image_name"] as! String
                imagextension = (imageobj["image_type"] as! String)
                let imageURL = URL(string: (imageobj["image_link"] as! String))!
                if let dataimage = (try? Data(contentsOf: imageURL)) {
                    let image = UIImage(data: dataimage)
                    self.img!.image = image
                    let ratio = image!.size.width / image!.size.height
                    let newHeight = img.frame.width / ratio
                    constraintHeight.constant = newHeight
                    print(" N E W H E I G H T ", newHeight)
                    
                    view.layoutIfNeeded()
                    self.dismisALert()
                    let object_arr = imageobj["objects"] as! [Any]
                    ////print("\n",imageobj,"\n",imagename,"\n",imagextension,"\n",object_arr)
                    if object_arr.count != 0 {
                        for i in self.objectcount ..< object_arr.count {
                            loadObjectOnline(withobject_arr: object_arr, withobject_index: i, withOnline: self.Online, withOfflineobject: [])
                            objectcount = i + 1
                            if objectcount == object_arr.count  {
                                ////print("^^^^^^^^^^^^^^^^^^OBJECT SES^^^^^^^^^^^^^^^")
                                self.objectEndFlag = true
                            }
                            break
                        }
                    }
                }
            
            break;
            
            
            
        case false:
            //MARK: - Load Offline Images

            let OfflineImage = OfflineimageObj[0]
            imagename = OfflineImage.image_name!
            imagextension = OfflineImage.image_type!
            let image = UIImage(data: OfflineImage.image_file!)
                self.img!.image = image
            let object_arr = self.arrOfTotalObjects![self.currentcount]
                if object_arr.count != 0 {
                    for i in self.objectcount ..< object_arr.count {
                        loadObjectOffline(withOnline: self.Online, withOfflineobject: [object_arr[i]])
                        objectcount = i + 1
                        if objectcount == object_arr.count  {
                            ////print("^^^^^^^^^^^^^^^^^^OBJECT SES^^^^^^^^^^^^^^^")
                            self.objectEndFlag = true
                        }
                        break
                    }
                }
            
            
            break
            ////print("")
        }
        
    }
    
    //MARK: - Load Online Objects
    //MARK: - ############################################### O N L I N E  METHOD 3 ####################################

    private func loadObjectOnline(withobject_arr object_arr:[Any],withobject_index object_index:Int,withOnline online: Bool,withOfflineobject offlineObject: [Objects]) {
        if self.Online == true {
        loadRadio(radioURL: ((object_arr[object_index] as! [String:Any])["textToSpeech"] as! String))
          objectname = ((object_arr[object_index] as! [String:Any])["object_name"] as! String)
        objectflag = "\((object_arr[object_index] as! [String:Any])["flag"] as! NSNumber)"
         self.text_to_speech = ((object_arr[object_index] as! [String:Any])["textToSpeech"] as! String)
        }else{
            loadObjectOffline(withOnline: self.Online, withOfflineobject: offlineObject)
        }

    }

    //MARK: - ############################################### O N L I N E DETCET METHOD 2 ####################################

    //MARK:- Load next images
    private func loadNextImage(withboxview boxview: UIView){
        boxview.removeFromSuperview()
        self.offline_box_view.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
        if self.Online == true {
        if self.objectEndFlag == true {
        self.currentcount = self.currentcount + 1
            self.loadImage(withindex: self.currentcount, withimage: self.arrImageResponse, withOnline: self.Online, withOfflineImage: [])
        }else{
        self.wrongCount = 0
            self.loadimageObject(withimageobj: self.imgObj, withOnline: self.Online, withOfflineImageObj: [] )
        }
        }else{
            if self.objectEndFlag == true {
            self.currentcount = self.currentcount + 1
                self.loadImage(withindex: self.currentcount, withimage: self.arrImageResponse, withOnline: self.Online, withOfflineImage: self.arrOfflineImages!)
            }else{
            self.wrongCount = 0
                self.loadimageObject(withimageobj: self.imgObj, withOnline: self.Online, withOfflineImageObj: [(self.imgOfflineObj!)] )
            }

        }
    }
    //MARK: - ############################################### O N L I N E  DETECT METHOD 1 ####################################

    
    //MARK: - Failure attempt for wrong detection
    func FailureAttempt(withboundingboxarr boundingboxarr:[Any], withOnline online:Bool, withOfflineBoundingBox offlineboundingbox:[Any]){
        if online == true {
        if boundingboxarr.count != 0 {
        if self.wrongCount < 3 {
            self.loadRadio(radioURL: self.text_to_speech)
        }else {
            let imgframe = self.img.layer.frame
            var boxview = UIView(frame: imgframe)
            self.img.addSubview(boxview)
            self.img.bringSubviewToFront(boxview)

        if self.wrongCount == 3 {
            self.view.isUserInteractionEnabled = false
            
            for i in 0 ..< boundingboxarr.count {
                let boxobj = boundingboxarr[i] as! [String:Any]
                let boundingBoxRight = boxobj["rightBottm"] as! String
                let boundingBoxLeft = boxobj["leftTop"] as! String
                let restx = boundingBoxRight.components(separatedBy: ",")
                let y = restx[1].components(separatedBy: " ")

                let restw = boundingBoxLeft.components(separatedBy: ",")
                let x = restw[1].components(separatedBy: " ")
                ////print(boundingBoxRight,"\n",boundingBoxLeft)
                ////print("x: \n",(Float(restw[0])!))
                ////print("y: \n",((Float(x[1])!)))
                ////print("width: \n",((Float(restx[0]))! - (Float(restw[0]))!),"=",(Float(restx[0])),(Float(restw[0])))
                ////print("height: \n",((Float(y[1]))! - (Float(x[1]))!),"=",(Float(y[1])),(Float(x[1])))

                let myView = UIView(frame: CGRect(x: CGFloat((Float(restw[0])!)), y: CGFloat((Float(x[1])!)), width: CGFloat((Float(restx[0]))! - (Float(restw[0]))!), height: CGFloat((Float(y[1]))! - (Float(x[1]))!)))
                myView.backgroundColor = .clear
                myView.layer.borderWidth = 5
                myView.layer.borderColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
                myView.startGlowing(
                    color: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1),
                    duration: 0.5)
                //////print("x",CGFloat(Float(restw[0])!),"y",CGFloat(Float(x[1])!),"width",CGFloat((Float(restx[0]))! - (Float(restw[0]))!),"height",CGFloat((Float(y[1]))! - (Float(x[1]))!))
                boxview.addSubview(myView)

            }
            

            let url = Bundle.main.url(forResource: "Sparkle-sound-effect", withExtension: "mp3")
            let playerItem = AVPlayerItem(url: url!)
            let player=AVPlayer(playerItem: playerItem)
            let playerLayer=AVPlayerLayer(player: player)
            playerLayer.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            self.view.layer.addSublayer(playerLayer)
            player.play()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 7.0) {
                self.loadNextImage(withboxview: boxview)
            }
        }
            }
        }
        }else{
            if self.wrongCount < 3 {
                    self.prepareYourSound(myData: self.offline_text_to_speech)
                    self.myPlayer.play()

                
            }else {
                let imgframe = self.img.layer.frame
                var boxview = UIView(frame: imgframe)
                self.img.addSubview(boxview)
                self.img.bringSubviewToFront(boxview)

            if self.wrongCount == 3 {
                self.view.isUserInteractionEnabled = false
                let url = Bundle.main.url(forResource: "Sparkle-sound-effect", withExtension: "mp3")
                let playerItem = AVPlayerItem(url: url!)
                let player=AVPlayer(playerItem: playerItem)
                let playerLayer=AVPlayerLayer(player: player)
                playerLayer.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
                self.view.layer.addSublayer(playerLayer)
                player.play()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 7.0) {
                    self.loadNextImage(withboxview: boxview)
                }
                 self.makeOfflineBoundingBox(withboundingrightbottm: self.currentOfflineObject!.boundingrightbottm!, withboundingboxlefttop: self.currentOfflineObject!.boundingboxlefttop!, withshow: true)
            }
            }
        }
    }
   
}









private typealias OfflineExtensions = FindObjectVC
extension OfflineExtensions {
    //MARK:- GET Offline Images
//MARK: - ######################################### O F F L I N E ##############################
func GetOfflineImages(){
    var myarr = [Images]()
    var objcetid = [[Objects]]()
    var myobjarr = [Objects]()
    myarr = CoreDataHelper.retrieveImageData(withEntityname: ImageEntity, withcolumnname: "", withsortwithCategoryType: cat_type)
    if myarr.count != 0 {
        for i in 0 ..< myarr.count{
            self.arrOfflineImages = myarr
          //  myobjarr = CoreDataHelper.retrieveObjectData(withEntityname: ObjectEntity, withcolumnname: "", withsortwithImage_id: myarr[i].id)
            objcetid.append(myobjarr)
            
        }
        self.currentcount = 0
        self.imagecount = (self.arrOfflineImages!.count)
        self.arrOfObjects = myobjarr
        self.arrOfTotalObjects = objcetid
        
        
        self.loadImage(withindex: 0, withimage: [], withOnline: self.Online, withOfflineImage: self.arrOfflineImages!)
    }else{
        PopUpViewController.showPopup(onParentViewController: self, withSelectedItem: "", withalertTitle: "", withstatusType: .Nodata, withpopupType: .Double, withviewControllerType: .CategoryVC, selected: { (_, _) in
            self.navigationController?.popViewController(animated: true)
        })
    }
}
    private func makeOfflineBoundingBox(withboundingrightbottm boundingrightbottm:String,withboundingboxlefttop boundingboxlefttop:String,withshow show:Bool){
    let imgframe = self.img.layer.frame
             self.offline_box_view = UIView(frame: imgframe)
             self.img.addSubview(self.offline_box_view)
             self.img.bringSubviewToFront(self.offline_box_view)
             
                 let boundingBoxRight = boundingrightbottm
                 let boundingBoxLeft = boundingboxlefttop
                 let restx = boundingBoxRight.components(separatedBy: ",")
                 ////print(restx[0],restx[1])
                 let y = restx[1].components(separatedBy: " ")
                 ////print(y[0],y[1])

                 let restw = boundingBoxLeft.components(separatedBy: ",")
                 ////print(restw[0],restw[1])
                 let x = restw[1].components(separatedBy: " ")
                 ////print(x[0],x[1])

                 let myView = UIView(frame: CGRect(x: CGFloat((Float(restw[0])!)), y: CGFloat((Float(x[1])!)), width: CGFloat((Float(restx[0]))! - (Float(restw[0]))!), height: CGFloat((Float(y[1]))! - (Float(x[1]))!)))
                 myView.backgroundColor = .clear
            
             myView.tag = 101
        if show == true {
            myView.layer.borderWidth = 5
                           myView.layer.borderColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
                           myView.startGlowing(
                               color: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1),
                               duration: 0.5)
        }
                 //////print("x",CGFloat(Float(restw[0])!),"y",CGFloat(Float(x[1])!),"width",CGFloat((Float(restx[0]))! - (Float(restw[0]))!),"height",CGFloat((Float(y[1]))! - (Float(x[1]))!))
             self.offline_box_view.addSubview(myView)
    }
    
    //MARK: - Load Offline Objects
    private func loadObjectOffline(withOnline online: Bool,withOfflineobject offlineObject: [Objects]){
        self.currentOfflineObject = offlineObject[0]
        objectname = offlineObject[0].object_name!
       // objectflag = "\((object_arr[object_index] as! [String:Any])["flag"] as! NSNumber)"
        ////print((offlineObject[0].objectmp3_file! as NSData))
//        self.offline_text_to_speech = (offlineObject[0].objectmp3_file! as NSData)
//        self.prepareYourSound(myData: offlineObject[0].objectmp3_file! as NSData)
        self.myPlayer.play()
        
        self.makeOfflineBoundingBox(withboundingrightbottm: offlineObject[0].boundingrightbottm!, withboundingboxlefttop: offlineObject[0].boundingboxlefttop!, withshow: false)

        
    }
    //MARK: - ############################################### O F F L I N E  DETECT METHOD 1 ####################################

    private  func createQuestions(handleComplete:(()->())){
                           // do something
                           handleComplete() // call it when finished stuff what you want
                       }
}


