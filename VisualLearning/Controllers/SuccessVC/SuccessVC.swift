//
//  SuccessVC.swift
//  Kidsapp
//
//  Created by DebasmitaArchan on 28/01/20.
//  Copyright © 2020 DebasmitaArchan. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftGifOrigin
import ImageIO
import AVFoundation
class SuccessVC: UIViewController {
var arrurl = [URL]()
    var player : AVPlayer?
    var didSelect: ((_ selectedItem: String, _ index: Int?) -> Void)?
    var statusType : StatusType!
    var viewControllerType : ViewcontrollerType!
    @IBOutlet weak var imgtick: UIImageViewX!
    @IBOutlet weak var imggif: UIImageView!
    @IBOutlet weak var imgroundoff: UIImageView!
    @IBOutlet weak var imgconfetiright: UIImageView!
    @IBOutlet weak var imgconfetileft: UIImageView!
    @IBOutlet weak var imgsucces: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.global().async {
            let image = UIImage.gif(name: "cong")
            DispatchQueue.main.async {
                self.imggif.image = image
                self.imggif.animationRepeatCount = 1
            }
        }
        
        DispatchQueue.global().async {
            let image = UIImage.gif(name: "confetti_gif_right")
            DispatchQueue.main.async {
                self.imgconfetiright.image = image
                self.imggif.animationRepeatCount = 1
            }
        }
        DispatchQueue.global().async {
            let image = UIImage.gif(name: "tick")
            DispatchQueue.main.async {
                self.imgtick.image = image
                self.imggif.animationRepeatCount = 1
            }
        }
        DispatchQueue.global().async {
            let image = UIImage.gif(name: "giphy (4)")
            DispatchQueue.main.async {
                self.imgroundoff.image = image
                self.imggif.animationRepeatCount = 1
            }
        }
        
        DispatchQueue.global().async {
            let image = UIImage.gif(name: "confetti_gif_left")
            DispatchQueue.main.async {
                self.imgconfetileft.image = image
            }
        }
        DispatchQueue.global().async {
            let image = UIImage.gif(name: "confetti_ball_256")
            DispatchQueue.main.async {
                self.imgsucces.image = image
            }
        }
        DispatchQueue.global().async {
            let image = UIImage.gif(name: "cong")
            DispatchQueue.main.async {
                self.imggif.image = image
                self.imggif.animationRepeatCount = 1
            }
        }
        
        
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
 
   
    // Specify the orientation.
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        didSelect!("MostRecent",0)
        dismissAnimationonPickerView()
        
    }
    
    
    // MARK: - viewDidLayoutSubviews
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        // popupRoundView.roundedCorner(withCornerRadius: IS_IPAD() ? 150/2 : 100/2, borderWidth: 1.0, color: .white)
        //popUpContenerView.roundedCorner(withCornerRadius: 10, borderWidth: 1.0, color: .clear)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Or to rotate and lock
        AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
     //   popupMainContenerView.alpha = 0
      //  popupBackview.alpha = 0
       // popupMainContenerView.transform = CGAffineTransform(translationX: 0, y: -screenHeight)
        
    }
    

    

    
    func bounceFromTop() -> Void {
        
        
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions(rawValue: 0), animations: {
//            self.popupMainContenerView.alpha = 1.0
//            self.popupBackview.alpha = 0.8
//            self.popupMainContenerView.transform = CGAffineTransform.identity
//
        }, completion: nil)
    }
    
    
    static func showPopup(onParentViewController parentViewController: UIViewController, withSelectedItem selectedItem: String?,withalertTitle alertTitle:String, withstatusType statusType: StatusType,withpopupType popupType: PopupviewType,withviewControllerType viewControllerType: ViewcontrollerType, selected: @escaping (_ value: String?, _ index: Int?) -> Void) {
        
        print("Selected Item==\(selectedItem!)")
        let viewController =  UIStoryboard(name:"Main",bundle:nil)
        let obj1 = viewController.instantiateViewController(withIdentifier: "SuccessVC") as! SuccessVC
      //  obj1.statusType = statusType
       // obj1.viewControllerType = viewControllerType
        obj1.displaylogoutPickerView(withParentViewController: parentViewController, withSelectedItem: selectedItem, withalertTitle: alertTitle, withstatusType: statusType, withpopupType: popupType, withviewControllerType: viewControllerType)
        obj1.didSelect = selected
        
    }
    func displaylogoutPickerView(withParentViewController parentController: UIViewController, withSelectedItem selectItem: String?,withalertTitle alertTitle:String,withstatusType statusType: StatusType, withpopupType popupType: PopupviewType,withviewControllerType viewControllerType: ViewcontrollerType ) {
        
        self.view.frame = UIScreen.main.bounds
        UIApplication.shared.windows.first!.addSubview(self.view)
        parentController.addChild(self)
        self.didMove(toParent: parentController)
        parentController.view.bringSubviewToFront(self.view)
     //   self.lblMsg.text! = selectItem!
    //    self.popuptitle.text! = alertTitle
       // loadRadio(radioURL: "kids_cheering.mp3")
        let url = Bundle.main.url(forResource: "kids_cheering", withExtension: "mp3")
        
        let playerItem = AVPlayerItem(url: url!)
        let player=AVPlayer(playerItem: playerItem)
        let playerLayer=AVPlayerLayer(player: player)
        
        playerLayer.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        self.view.layer.addSublayer(playerLayer)
        player.play()
        
       
        DispatchQueue.global().async {
            let image = UIImage.gif(name: "confetti_gif_right")
            DispatchQueue.main.async {
                self.imgconfetiright.image = image
                self.imggif.animationRepeatCount = 1
            }
        }
        DispatchQueue.global().async {
            let image = UIImage.gif(name: "tick")
            DispatchQueue.main.async {
                self.imgtick.image = image
                self.imggif.animationRepeatCount = 1
            }
        }
        DispatchQueue.global().async {
            let image = UIImage.gif(name: "giphy (4)")
            DispatchQueue.main.async {
                self.imgroundoff.image = image
                self.imggif.animationRepeatCount = 1
            }
        }
        
        DispatchQueue.global().async {
            let image = UIImage.gif(name: "confetti_gif_left")
            DispatchQueue.main.async {
                self.imgconfetileft.image = image
            }
        }
        DispatchQueue.global().async {
            let image = UIImage.gif(name: "confetti_ball_256")
            DispatchQueue.main.async {
                self.imgsucces.image = image
            }
        }
        
        
        
        bounceFromTop()
        if UserDefaults.standard.bool(forKey: "offline") == true {
            DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
                self.didSelect!("MostRecent",0)
                self.dismissAnimationonPickerView()
            }
        }else{
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.didSelect!("MostRecent",0)
            self.dismissAnimationonPickerView()
        }
        }

    }
    
    private func dismissAnimationonPickerView() {
        
        UIView.animate(withDuration: 0.3, animations: {
//            self.popUpContenerView.alpha = 0
//            self.popupMainContenerView.alpha = 0
//            self.popupBackview.alpha = 0
//            self.popupMainContenerView.transform = CGAffineTransform(translationX: 0, y: screenHeight)
//
        }) { (_) in
            //            statusBarColorChange(withcolor: InvoiceScreenStatusColor)
            self.view.removeFromSuperview()
            self.removeFromParent()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //ok button
    
    @IBOutlet weak var imghightolow: UIImageView!
    @IBOutlet weak var imgmostrect: UIImageView!
    
    @IBOutlet weak var imgprichehigh: UIImageView!
    
    
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

