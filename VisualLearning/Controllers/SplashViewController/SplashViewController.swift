//
//  SplashViewController.swift
//  Kidsapp
//
//  Created by DebasmitaArchan on 01/03/20.
//  Copyright © 2020 DebasmitaArchan. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let mDashBoardViewController = storyBoard.instantiateViewController(withIdentifier: "LandingVC") as! LandingVC
            self.navigationController?.pushViewController(mDashBoardViewController, animated: true)

        }
        // Do any additional setup after loading the view.
    }
    override open var shouldAutorotate: Bool {
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //  AppUtility.lockOrientation(.landscape)
        // Or to rotate and lock
        AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        
    }
    
    
    
    // Specify the orientation.
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
