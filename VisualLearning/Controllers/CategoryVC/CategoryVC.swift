//
//  CategoryVC.swift
//  Kidsapp
//
//  Created by DebasmitaArchan on 18/01/20.
//  Copyright © 2020 DebasmitaArchan. All rights reserved.
//

import UIKit
import AVFoundation
var no_internet = false
class CategoryVC: UIViewController,FooTwoViewControllerDelegate {
    @IBOutlet weak var bsckview: UICollectionView!
    @IBOutlet weak var collcategory: UICollectionView!
    @IBOutlet weak var backview: UIViewX!
    weak var delegate: FooTwoViewControllerDelegate?
    var arrcategory = [Any]()
    var Online = true
    var arrayOfCategory = [Category]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collcategory.delegate = self
        self.collcategory.dataSource = self
        self.collcategory.reloadData()
        
        self.backview.layer.cornerRadius = self.backview.frame.height / 2;
        if  UserDefaults.standard.value(forKey: "offline") != nil {
            if UserDefaults.standard.bool(forKey: "offline") == false {
                self.Online = true
                getCategory()
            }else{
                self.Online = false
            }
        }

       
        let width = (view.frame.width-140)/3
        let layout = collcategory.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: width-50)
        
        // Do any additional setup after loading the view.
    }
    override open var shouldAutorotate: Bool {
        return false
    }
    
    @IBAction func clickexit(_ sender: Any) {
          exit(0)
      }
    var players = AVPlayer()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let url = Bundle.main.url(forResource: "CHEERFUL_THEME_A_j_j_schutz_HIB307_LC06881", withExtension: "mp3")
        let playerItem = AVPlayerItem(url: url!)
         self.players = AVPlayer(playerItem: playerItem)
        let playerLayer=AVPlayerLayer(player: players)
        playerLayer.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        self.view.layer.addSublayer(playerLayer)
        
       // players.play()
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object:
        players.currentItem, queue: .main) { [weak self] _ in
            self?.players.seek(to: CMTime.zero)
            //self?.players.play()
        }
        AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)

    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(true)
     //   self.players.pause()
    }
  
    // Specify the orientation.
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }
    @IBAction func clickprev(_ sender: Any) {
        let collectionBounds = self.collcategory.bounds
        let contentOffset = CGFloat(floor(self.collcategory.contentOffset.x - collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    @IBAction func clicknext(_ sender: Any) {
        let collectionBounds = self.collcategory.bounds
        let contentOffset = CGFloat(floor(self.collcategory.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    func moveCollectionToFrame(contentOffset : CGFloat) {
        
        let frame: CGRect = CGRect(x : contentOffset ,y : self.collcategory.contentOffset.y ,width : self.collcategory.frame.width,height : self.collcategory.frame.height)
        self.collcategory.scrollRectToVisible(frame, animated: true)
    }
    @IBAction func clickback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func myVCDidFinish(error: String) {
        PopUpViewController.showPopup(onParentViewController: self, withSelectedItem: "", withalertTitle: "", withstatusType: .Nointernet, withpopupType: .Double, withviewControllerType: .CategoryVC) { (_, _) in
           
            self.getCategory()
        }
    }
    


}
private typealias ApiCalling = CategoryVC
extension ApiCalling {

    
    
    func getCategory(){
        LoaderPopUpViewController.showPopup(onParentViewController: self, withSelectedItem: [], withapicallType: .Category, withstatusType: .Other, withpopupType: .Single, withviewControllerType: .CategoryVC) { (resp, err) in
            if err!.count == 0 {
                print(resp)
                if  let Main_response = resp as? [String: Any] {
                let status  = Main_response["status"] as! NSNumber
                if status == 0 {
                    if let Response = Main_response["Response"] as? [Any]{
                        if Response.count != 0 {
                            self.arrcategory = Response
                            self.collcategory.delegate = self
                            self.collcategory.dataSource = self
                            self.collcategory.reloadData()
                        }
                    }
                } else  if status == 1 {
                    if let response = Main_response["response"] as? [String:Any]{
                        if let message = response["message"] as? String{
                            
                        }
                        
                    }
                    
                }  else {
                }
                }else{
                    PopUpViewController.showPopup(onParentViewController: self, withSelectedItem: "", withalertTitle: "", withstatusType: .Nointernet, withpopupType: .Double, withviewControllerType: .CategoryVC, selected: { (_, _) in
                        self.getCategory()
                    })
                }
            }else{
                
            }
        }
    }
    
    
}
private typealias CollectionViewDelegate = CategoryVC
extension CollectionViewDelegate: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        // In this function is the code you must implement to your code project if you want to change size of Collection view
        let width  = (view.frame.width-150)/3
        return CGSize(width: width, height: 100)
    }
func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
}
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
     if  (UserDefaults.standard.value(forKey: "offline") == nil) ||  (UserDefaults.standard.bool(forKey: "offline") == false){
        return arrcategory.count

    }
    return arrayOfCategory.count
   // return 10

}
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as? CategoryCell
    if  (UserDefaults.standard.value(forKey: "offline") == nil) ||  (UserDefaults.standard.bool(forKey: "offline") == false){
    cell?.lblcategoryname.text = (((arrcategory[indexPath.item] as! [String:Any])["name"] as! String).uppercased())
    cell?.btncategoryname.tag = indexPath.item
    cell?.imgCategory.sd_setImage(with: URL(string: ((arrcategory[indexPath.item] as! [String:Any])["image_path"] as! String)), placeholderImage: #imageLiteral(resourceName: "placeholder"))
    cell?.btncategoryname.addTarget(self, action: #selector(self.buttonClicked(sender:)), for:.touchUpInside)
    
    cell?.lblcategoryname.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    cell?.lblcategoryname.shadowOffset = CGSize(width: -3.0, height: 3.0)
    
    cell?.lblcategoryname.adjustsFontSizeToFitWidth = true
    
    if ((arrcategory[indexPath.item] as! [String:Any])["name"] as! String) ==  "Flower" {
      cell?.alpha = 0.45
        cell?.isUserInteractionEnabled = false
    }else{
        cell?.alpha = 1
        cell?.isUserInteractionEnabled = true
    }
    }else{
//        cell?.lblcategoryname.text = ((self.arrayOfCategory[indexPath.item].name).uppercased())
//           cell?.btncategoryname.tag = indexPath.item
//        cell?.imgCategory.image =  UIImage(data:((self.arrayOfCategory[indexPath.item].category_image)!))
//           cell?.btncategoryname.addTarget(self, action: #selector(self.buttonClicked(sender:)), for:.touchUpInside)
//
//           cell?.lblcategoryname.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
//           cell?.lblcategoryname.shadowOffset = CGSize(width: -3.0, height: 3.0)
//
//           cell?.lblcategoryname.adjustsFontSizeToFitWidth = true
//
//           if (self.arrayOfCategory[indexPath.item].name) ==  "Flower" {
//             cell?.alpha = 0.45
//               cell?.isUserInteractionEnabled = false
//           }else{
//               cell?.alpha = 1
//               cell?.isUserInteractionEnabled = true
//           }
    }
    
    return cell!
}
    
    @objc func buttonClicked(sender:UIButton)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let mDashBoardViewController = storyBoard.instantiateViewController(withIdentifier: "FindObjectVC") as! FindObjectVC
        if self.Online == true {
        mDashBoardViewController.cat_id = ((arrcategory[sender.tag] as! [String:Any])["id"] as! NSNumber)
        mDashBoardViewController.arrcategory = (arrcategory)
        }else{
           // mDashBoardViewController.cat_type = (self.arrayOfCategory[sender.tag].name!)
        }
        self.navigationController?.pushViewController(mDashBoardViewController, animated: true)

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
    }

}






class CategoryCell: UICollectionViewCell {
    @IBOutlet weak var btncategoryname: UIButton!
    @IBOutlet weak var lblcategoryname: UILabelX!
    @IBOutlet weak var imgCategory: UIImageView!
    
    var viewController = UIViewController()
    var imagePicker = UIImagePickerController()
    var data = Data()
    
    override func awakeFromNib() {
        super.awakeFromNib()
}
}
private typealias CoreDataMethods = CategoryVC
