//
//  CoreDataHelper.swift
//  VisualLearning
//
//  Created by Debasmita on 09/04/20.
//  Copyright © 2020 Debasmita. All rights reserved.
//

import Foundation
import CoreData
import UIKit
class CoreDataHelper {
  static  var delegate : BackgroundDelegate?

    //MARK:- ================== Save data for category entity ================
        static func createDataForCategoryEntity(withCategorydata categoryResponse:[Any]){
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        //Now let’s create an entity and new user records.
        let categoryEntity = NSEntityDescription.entity(forEntityName: CategoryEntity, in: managedContext)!

        //final, we need to add some data to our newly created record for each keys using
        //here adding 5 data with loop

        
        for i in 0 ..< categoryResponse.count {
             let category = NSManagedObject(entity: categoryEntity, insertInto: managedContext)
            category.setValue(((categoryResponse[i] as! [String:Any])["id"] as! NSNumber), forKeyPath: "id")
            category.setValue(((categoryResponse[i] as! [String:Any])["image_name"] as! String), forKeyPath: "image_name")
            category.setValue(((categoryResponse[i] as! [String:Any])["image_path"] as! String), forKeyPath: "image_path")
            category.setValue(((categoryResponse[i] as! [String:Any])["name"] as! String), forKeyPath: "name")
              category.setValue(((categoryResponse[i] as! [String:Any])["image_ext"] as! String), forKeyPath: "image_ext")
             let imageURL = URL(string: ((categoryResponse[i] as! [String:Any])["image_path"] as! String))!
            if let dataimage = (try? Data(contentsOf: imageURL)) {
                 category.setValue(dataimage, forKey: "category_image")
                print("CATEGORY DOWNLOAD FINISH ", i)
                var initialViewController:UIViewController?
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                initialViewController = storyboard.instantiateViewController(withIdentifier: "LandingVC") as! LandingVC
                    delegate = initialViewController as? BackgroundDelegate
                    delegate?.didEnterBackground()

            }
            createDataForImagesEntity(withImagedata: ((categoryResponse[i] as! [String:Any])["image_details"] as! [Any]), withcat_id: ((categoryResponse[i] as! [String:Any])["id"] as! Int64))
        }
        
        //Now we have set all the values. The next step is to save them inside the Core Data
        
        do {
            try managedContext.save()
           
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    //MARK:- ================== Save data for Images entity ================

    static func createDataForImagesEntity(withImagedata arrImageResponse:[Any],withcat_id cat_id:Int64){
    
    //As we know that container is set up in the AppDelegates so we need to refer that container.
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
    
    //We need to create a context from this container
    let managedContext = appDelegate.persistentContainer.viewContext
    let managedObjectContext = appDelegate.persistentContainer.viewContext as! NSManagedObjectContext
    let contxt = managedObjectContext
    //Now let’s create an entity and new user records.
    let imageEntity = NSEntityDescription.entity(forEntityName: ImageEntity, in: managedContext)!
    let objectEntity = NSEntityDescription.entity(forEntityName: ObjectEntity, in: contxt)!

    //final, we need to add some data to our newly created record for each keys using
    //here adding 5 data with loop

    
    for i in 0 ..< arrImageResponse.count {
         let images = NSManagedObject(entity: imageEntity, insertInto: managedContext)
        images.setValue(cat_id, forKeyPath: "category_id")
        images.setValue(((arrImageResponse[i] as! [String:Any])["id"] as! Int64), forKeyPath: "id")
        images.setValue(((arrImageResponse[i] as! [String:Any])["image_name"] as! String), forKeyPath: "image_name")
        images.setValue(((arrImageResponse[i] as! [String:Any])["image_link"] as! String), forKeyPath: "image_link")
        images.setValue(((arrImageResponse[i] as! [String:Any])["image_type"] as! String), forKeyPath: "image_type")
        images.setValue(((arrImageResponse[i] as! [String:Any])["category"] as! String), forKeyPath: "category")
        let imageURL = URL(string: ((arrImageResponse[i] as! [String:Any])["image_link"] as! String))!
                   if let dataimage = (try? Data(contentsOf: imageURL)) {
                        images.setValue(dataimage, forKey: "image_file")
                    print("IMAGE DOWNLOAD FINISH ", i)
                   }
        
        if ((arrImageResponse[i] as! [String:Any])["object_details"] as! [String:Any]).count != 0 {
            for j in 0 ..< (((arrImageResponse[i] as! [String:Any])["object_details"] as! [Any]).count) {

                let arrObject = ((arrImageResponse[i] as! [String:Any])["object_details"] as! [Any])

            let objects = NSManagedObject(entity: objectEntity, insertInto: managedContext)
                objects.setValue(cat_id, forKeyPath: "category_id")
                objects.setValue(((arrImageResponse[i] as! [String:Any])["category"] as! String), forKeyPath: "category")
            objects.setValue(((arrImageResponse[i] as! [String:Any])["id"] as! Int64), forKeyPath: "id")
            objects.setValue(((arrObject[j] as! [String:Any])["object_name"] as! String), forKeyPath: "object_name")
                 objects.setValue(((arrObject[j] as! [String:Any])["object_id"] as! String), forKeyPath: "object_id")
                 objects.setValue(((arrObject[j] as! [String:Any])["BoundingBoxLeftTop"] as! String), forKeyPath: "boundingboxlefttop")
                 objects.setValue(((arrObject[j] as! [String:Any])["BoundingRightBottm"] as! String), forKeyPath: "boundingrightbottm")
                 objects.setValue(((arrObject[j] as! [String:Any])["textToSpeech"] as! String), forKeyPath: "texttospeech")
                
              
                            var urlWebView = NSURL(string: ((arrObject[j] as! [String:Any])["textToSpeech"] as! String))!
                        var requestWebView = NSURLRequest(url: urlWebView as URL)
                        var musicdata = Data()
                        NSURLConnection.sendAsynchronousRequest(requestWebView as URLRequest, queue: OperationQueue.main, completionHandler: {
                                response, data, error in

                                if error != nil {

                                    print("There was an error")

                                } else {

                                    let musicFile = (data)
                                    musicdata = musicFile!
                                    var documentsDirectory:String?

                                    var paths:[AnyObject] = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true) as [AnyObject]

                                    if paths.count > 0 {

                                        documentsDirectory = paths[0] as? String

                                        var savePath = documentsDirectory! + "/\(((arrObject[j] as! [String:Any])["object_name"] as! String)).mp3"

                                        FileManager.default.createFile(atPath: savePath, contents: data, attributes: nil)
                                        objects.setValue(musicFile!, forKey: "objectmp3_file")
                                        print("\n objectmp3_file", musicFile!,((arrObject[j] as! [String:Any])["object_name"] as! String))
                //                        self.prepareYourSound(myData: musicFile as! NSData)
                //                        self.myPlayer.play()
                                        //tried to play it here but i cant since savePath is a string and not actually audio file

                                        // list your files from disk (documents)

                                        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                                        let files = FileManager().enumerator(atPath: documentsPath)
                                        var myFiles:[String] = []
                                        while let file: AnyObject = files?.nextObject() as AnyObject? {
                                            myFiles.append(file as! String)
                                            print("\n\(file as! String)", musicFile!)
                                        }
                                        
                                        do {
                                               try managedContext.save()
                                              
                                           } catch let error as NSError {
                                               print("Could not save. \(error), \(error.userInfo)")
                                           }

                                    }

                                }


                            })
                        
            }
        }

    }
    //Now we have set all the values. The next step is to save them inside the Core Data
    
    do {
        try managedContext.save()
       
    } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
    }
}
    
    
    
    //MARK:- ================== Fetch data for entity ================
    fileprivate var mainContextInstance: NSManagedObjectContext!

    
    static func retrieveData(withEntityname entityname: String,withcolumnname columnname: String) -> [Category] {
            
            //As we know that container is set up in the AppDelegates so we need to refer that container.
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return []}
            let managedContext = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityname)
            do {
                let sort = NSSortDescriptor(key: "id", ascending: true)
                fetchRequest.sortDescriptors = [sort]
                let result = try managedContext.fetch(fetchRequest) as! [Category]

                return result
            } catch {
                
                print("Failed")
            }
        return []
        }
    
    
  
    static func retrieveImageData(withEntityname entityname: String,withcolumnname columnname: String,withsortwithCategoryType sortwithCategoryType :String) -> [Images] {
            
            //As we know that container is set up in the AppDelegates so we need to refer that container.
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return []}
            let managedContext = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityname)
            let predicate = NSPredicate(format: "category == %@", sortwithCategoryType)

            do {
                let sort = NSSortDescriptor(key: "id", ascending: true)
                fetchRequest.sortDescriptors = [sort]
                fetchRequest.predicate = predicate
                let result = try managedContext.fetch(fetchRequest) as! [Images]

                return result
            } catch {
                
                print("Failed")
            }
        return []
        }
    
    static func retrieveObjectData(withEntityname entityname: String,withcolumnname columnname: String,withsortwithImage_id sortwithImage_id :Int64) -> [Objects] {
            
            //As we know that container is set up in the AppDelegates so we need to refer that container.
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return []}
            let managedContext = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityname)
            let predicate = NSPredicate(format: "id == \(sortwithImage_id)")

            do {
                let sort = NSSortDescriptor(key: "id", ascending: true)
                fetchRequest.sortDescriptors = [sort]
                fetchRequest.predicate = predicate
                let result = try managedContext.fetch(fetchRequest) as! [Objects]

                return result
            } catch {
                
                print("Failed")
            }
        return []
        }
    
    
    //MARK:- ================== Delete data for  entity ================

    static  func deleteData(withEntityname entityname: String) {
        let appDel:AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context:NSManagedObjectContext = appDel.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityname)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try context.fetch(fetchRequest)
            for managedObject in results {
                if let managedObjectData: NSManagedObject = managedObject as? NSManagedObject {
                    context.delete(managedObjectData)
                }
            }
        } catch let error as NSError {
            print("Deleted all my data in myEntity error : \(error) \(error.userInfo)")
        }
    }
    
}
