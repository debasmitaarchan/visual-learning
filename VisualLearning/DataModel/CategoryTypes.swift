//
//  CategoryTypes.swift
//  VisualLearning
//
//  Created by Debasmita on 10/04/20.
//  Copyright © 2020 Debasmita. All rights reserved.
//

import Foundation


/**
    Enum for holding different entity type names (Coredata Models)
*/
enum CategoryTypes: String {
    case Category = "Category"
    
    static let getAll = [Category] //[Event, Foo,Bar]
}
